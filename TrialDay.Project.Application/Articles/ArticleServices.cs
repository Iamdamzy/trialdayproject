﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Abp.Timing;
using Castle.Core.Logging;
using Microsoft.EntityFrameworkCore;

namespace TrialDay.Project.Articles.Dtos
{
	public class ArticleServices : ProjectAppServiceBase, IArticleServices
	{
		private readonly IRepository<Article> _articleRepository;
		public new ILogger Logger { get; set; }

		public ArticleServices(IRepository<Article> articleRepository)
		{
			_articleRepository = articleRepository;
			Logger = NullLogger.Instance;
		}

		public async Task<Article> CreateArticle(CreateArticleInputDto input)
		{
			try
			{
				Article @article = new Article
				{
					Title = input.Title,
					Abstract = input.Abstract,
					AuthorName = input.Authors,
					Description = input.Description,
					PublicationDate = input.PublicationDate,
					Source = input.Source,
					CreationTime = Clock.Now
				};

				var task = await _articleRepository.InsertAsync(@article);
				return task;
			}
			catch(Exception ex)
			{
				Logger.Info(ex.Message);
				return null;
			}
		}

		public async Task<string> DeleteArticle(int Id)
		{
			try
			{
				var checkDataToUpdate = await _articleRepository.GetAll().AnyAsync(t => t.Id == Id);
				if (checkDataToUpdate)
				{
					var @articletobedeleted = _articleRepository.Get(Id);
					await _articleRepository.DeleteAsync(@articletobedeleted);
					return "Article with ID: " + Id + " Successfully Deleted";
				}
				return "No Article with " + Id + " Found";
			}
			catch(Exception ex)
			{
				Logger.Info("There is an Issue with Delete Article" + " " + ex.Message);
				return null;
			}
		}

		public async Task<List<Article>> GetAllArticle()
		{
			var result = await _articleRepository.GetAll().ToListAsync();

			return result;
		}

		public async Task<Article> UpdateArticle(Article input)
		{
			try
			{
				var checkDataToUpdate = await _articleRepository.GetAll().AnyAsync(t => t.Id == input.Id);
				if (checkDataToUpdate)
				{
					var @articletobeupdated = ObjectMapper.Map<Article>(input);
					var result = await _articleRepository.UpdateAsync(@articletobeupdated);
					return result;
				}
				return null;
			}catch(Exception ex)
			{
				Logger.Info("There is an Issue with UpdateAritcle" + " " + ex.Message);
				return null;
			}
		}

		public async Task<Article> GetArticleByID(int Id)
		{
			var exists = await _articleRepository.GetAll().AnyAsync(t => t.Id == Id);
			if (exists)
			{
				var @article = _articleRepository.Get(Id);
				return @article;
			}

			return null;
		}
	}
}
