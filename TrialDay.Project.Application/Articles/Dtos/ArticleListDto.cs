﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TrialDay.Project.Articles.Dtos
{
	public class ArticleListDto
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public string Abstract { get; set; }

		public string Authors { get; set; }
		public DateTime CreationTime { get; set; }

		public DateTime PublicationDate { get; set; }

		public string Source { get; set; }

		public string Description { get; set; }
	}
}
