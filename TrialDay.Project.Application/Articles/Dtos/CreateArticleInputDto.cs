﻿using Abp.AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace TrialDay.Project.Articles.Dtos
{
	[AutoMapFrom(typeof(Article))]
	public class CreateArticleInputDto
	{
		public string Title { get; set; }
		public string Abstract { get; set; }

		public string Authors { get; set; }
		public DateTime CreationTime { get; set; }

		public DateTime PublicationDate { get; set; }

		public string Source { get; set; }

		public string Description { get; set; }
	}
}
