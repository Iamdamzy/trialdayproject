﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrialDay.Project.Articles.Dtos;

namespace TrialDay.Project.Articles
{
	public interface IArticleServices : IApplicationService
	{
		Task<List<Article>> GetAllArticle();

		Task<Article> CreateArticle(CreateArticleInputDto input);

		Task<Article> UpdateArticle(Article input);

		Task<string> DeleteArticle(int Id);

		Task<Article> GetArticleByID(int Id);
	}
}
