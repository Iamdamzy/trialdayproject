﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TrialDay.Project.Authorization.Accounts.Dto;

namespace TrialDay.Project.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
