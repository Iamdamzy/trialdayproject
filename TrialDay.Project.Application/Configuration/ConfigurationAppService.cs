﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using TrialDay.Project.Configuration.Dto;


namespace TrialDay.Project.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ProjectAppServiceBase, IConfigurationAppService
    {
        public async System.Threading.Tasks.Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
