﻿using System.Threading.Tasks;
using TrialDay.Project.Configuration.Dto;

namespace TrialDay.Project.Configuration
{
    public interface IConfigurationAppService
    {
		System.Threading.Tasks.Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
