﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TrialDay.Project.MultiTenancy.Dto;

namespace TrialDay.Project.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

