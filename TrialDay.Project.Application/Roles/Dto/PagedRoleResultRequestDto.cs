﻿using Abp.Application.Services.Dto;

namespace TrialDay.Project.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

