﻿using System.Threading.Tasks;
using Abp.Application.Services;
using TrialDay.Project.Sessions.Dto;

namespace TrialDay.Project.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
