﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.Text;
using TrialDay.Project.TaskEntity;

namespace TrialDay.Project.Task.Dtos
{
	[AutoMapFrom(typeof(Tasks))]
	public class TaskListDto : EntityDto , IHasCreationTime
	{
		public string Title { get; set;}
		public string Description { get; set; }
		public DateTime CreationTime { get; set;  }

		public TaskState State { get; set; }

	}

	public class GetAllTaskInput
	{
		public TaskState? State { get; set; }
	}

	[AutoMapFrom(typeof(Tasks))]
	public class AddTaskInput 
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime CreationTime { get; set; }
		public TaskState State { get; set; }

	}

	public class TaskOutputDto
	{
		public string Message { get; set; }
	}
}
