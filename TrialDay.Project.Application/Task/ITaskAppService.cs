﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TrialDay.Project.Task.Dtos;
using TrialDay.Project.TaskEntity;

namespace TrialDay.Project.Task
{
	interface ITaskAppService : IApplicationService
	{
		Task<ListResultDto<TaskListDto>> GetAll(GetAllTaskInput input);

		Task<Tasks> AddTask(AddTaskInput input);

		Task<Tasks> UpdateTask(Tasks input);
	}
}
