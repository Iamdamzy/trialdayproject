﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Abp.Collections.Extensions;
using Abp.Domain.Repositories;
using Abp.Linq.Extensions;
using Abp.Timing;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TrialDay.Project.Task.Dtos;
using TrialDay.Project.TaskEntity;

namespace TrialDay.Project.Task
{
	public class TasksServices : ProjectAppServiceBase, ITaskAppService
	{
		private readonly IRepository<Tasks> _taskrepository;
		public TasksServices(IRepository<Tasks> taskrepository)
		{
			_taskrepository = taskrepository;
		}

		public async Task<ListResultDto<TaskListDto>> GetAll(GetAllTaskInput input)
		{
			var tasks = await _taskrepository
				.GetAll()
				.WhereIf(input.State.HasValue, t => t.State == input.State.Value)
				.OrderByDescending(t => t.CreationTime)
				.ToListAsync();

			return new ListResultDto<TaskListDto>(
				ObjectMapper.Map<List<TaskListDto>>(tasks)
				);
		}

		public async Task<Tasks> AddTask(AddTaskInput input)
		{
			try
			{
				Tasks @task = new Tasks
				{
					CreationTime = Clock.Now,
					Description = input.Description,
					State = input.State,
					Title = input.Title
				};
				var result = await _taskrepository.InsertAsync(@task);
				return result;
			}
			catch(Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}
			
		}

		public async Task<Tasks> UpdateTask(Tasks input)
		{
			
			try
			{
				var exists = await _taskrepository.GetAll().AnyAsync(t => t.Id == input.Id);
				if (exists)
				{
					var @tasktobeupdated = ObjectMapper.Map<Tasks>(input);
					var result =  _taskrepository.Update(@tasktobeupdated);
					return result;
				}
				return null;
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
				return null;
			}
			
		}
	}
}
