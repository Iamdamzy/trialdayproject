using System.ComponentModel.DataAnnotations;

namespace TrialDay.Project.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}