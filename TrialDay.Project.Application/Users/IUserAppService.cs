using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using TrialDay.Project.Roles.Dto;
using TrialDay.Project.Users.Dto;

namespace TrialDay.Project.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();

		System.Threading.Tasks.Task ChangeLanguage(ChangeUserLanguageDto input);
    }
}
