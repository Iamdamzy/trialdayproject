﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TrialDay.Project.Articles
{
	[Table("AppArticle")]
	public class Article: Entity, IHasCreationTime
	{
		public const int MaxTitleLength = 256;
		public const int MaxDescriptionLength = 64 * 1024; //64KB

		public string Title { get; set; }
		public string Abstract { get; set; }

		public string AuthorName { get; set; }
		public DateTime CreationTime { get; set; }

		public DateTime PublicationDate { get; set; }

		public string Source { get; set; }

		public string Description { get; set; }

		public Article()
		{
			CreationTime = Clock.Now;
		}

		//public Article(string title, string ab = null)
		//	: this()
		//{
		//	Title = title;
		//	Description = description;
		//}
	}


}

