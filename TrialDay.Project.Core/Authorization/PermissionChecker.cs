﻿using Abp.Authorization;
using TrialDay.Project.Authorization.Roles;
using TrialDay.Project.Authorization.Users;

namespace TrialDay.Project.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
