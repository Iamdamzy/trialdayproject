﻿using Abp.MultiTenancy;
using TrialDay.Project.Authorization.Users;

namespace TrialDay.Project.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
