﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using TrialDay.Project.Authorization.Roles;
using TrialDay.Project.Authorization.Users;
using TrialDay.Project.MultiTenancy;
using TrialDay.Project.TaskEntity;
using TrialDay.Project.Articles;

namespace TrialDay.Project.EntityFrameworkCore
{
    public class ProjectDbContext : AbpZeroDbContext<Tenant, Role, User, ProjectDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<Tasks> Tasks { get; set; }
		public DbSet<Article> Articles { get; set; }
        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
        }
    }
}
