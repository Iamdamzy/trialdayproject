﻿using Abp.AutoMapper;
using TrialDay.Project.Authentication.External;

namespace TrialDay.Project.Models.TokenAuth
{
    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
