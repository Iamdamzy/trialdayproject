﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using TrialDay.Project.Controllers;

namespace TrialDay.Project.Web.Controllers
{
    [AbpMvcAuthorize]
    public class AboutController : ProjectControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
