﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrialDay.Project.Articles;
using TrialDay.Project.Articles.Dtos;
using TrialDay.Project.Controllers;
using TrialDay.Project.Web.Models.Article;

namespace TrialDay.Project.Web.Controllers
{
	public class ArticleController : ProjectControllerBase
	{
		private readonly IArticleServices _articleServices;
		public ArticleController(IArticleServices articleServices)
		{
			_articleServices = articleServices;
		}

		public async Task<IActionResult> Index()
		{
			var result = (await _articleServices.GetAllArticle());
			List<ArticleListViewModel> model = new List<ArticleListViewModel>();
			foreach( var article in result)
			{
				ArticleListViewModel articleListView = new ArticleListViewModel
				{
					Abstract = article.Abstract,
					Authors = article.AuthorName,
					CreationTime = article.CreationTime,
					Description = article.Description,
					ID = article.Id,
					PublicationDate = article.PublicationDate,
					Source = article.Source,
					Title = article.Title
				};
				model.Add(articleListView);
			}
			return View(model);		
		}

		public JsonResult CreateArticle(CreateArticleInputDto input)
		{
			var @task =  _articleServices.CreateArticle(input).Result;
			if(@task.Title != null)
			{
				return Json(new { Id = 1});
			}
			return Json(new { Id = 0 });
		}
		
		public JsonResult UpdateArticle(Article input)
		{
			var @task = _articleServices.UpdateArticle(input).Result;
			if (@task.Title != null)
			{
				return Json(new { Id = 1 });
			}
			return Json(new { Id = 0 });
		}

		public JsonResult DeleteArticle(int input)
		{
			var @task = _articleServices.DeleteArticle(input).Result;
			if (@task.Length != 0)
			{
				return Json(new { Id = 1 });
			}
			return Json(new { Id = 0 });
		}




	}
}
