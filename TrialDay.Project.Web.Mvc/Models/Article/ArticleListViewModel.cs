﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TrialDay.Project.Articles.Dtos;

namespace TrialDay.Project.Web.Models.Article
{
	public class ArticleListViewModel
	{
		public int ID { get; set; }
		public string Title { get; set; }
		public string Abstract { get; set; }

		public string Authors { get; set; }
		public DateTime CreationTime { get; set; }

		public DateTime PublicationDate { get; set; }

		public string Source { get; set; }

		public string Description { get; set; }
	}
}
