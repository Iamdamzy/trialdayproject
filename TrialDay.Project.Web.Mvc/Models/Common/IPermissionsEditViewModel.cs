﻿using System.Collections.Generic;
using TrialDay.Project.Roles.Dto;

namespace TrialDay.Project.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}