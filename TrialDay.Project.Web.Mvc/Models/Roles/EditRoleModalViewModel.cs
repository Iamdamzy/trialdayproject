﻿using Abp.AutoMapper;
using TrialDay.Project.Roles.Dto;
using TrialDay.Project.Web.Models.Common;

namespace TrialDay.Project.Web.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class EditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool HasPermission(FlatPermissionDto permission)
        {
            return GrantedPermissionNames.Contains(permission.Name);
        }
    }
}
