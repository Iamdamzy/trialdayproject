﻿using System.Collections.Generic;
using TrialDay.Project.Roles.Dto;

namespace TrialDay.Project.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
