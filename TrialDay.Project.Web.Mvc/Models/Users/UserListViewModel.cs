using System.Collections.Generic;
using TrialDay.Project.Roles.Dto;
using TrialDay.Project.Users.Dto;

namespace TrialDay.Project.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
