﻿using TrialDay.Project.Configuration.Ui;

namespace TrialDay.Project.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
