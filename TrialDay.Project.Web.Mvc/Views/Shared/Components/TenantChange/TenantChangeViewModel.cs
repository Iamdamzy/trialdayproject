using Abp.AutoMapper;
using TrialDay.Project.Sessions.Dto;

namespace TrialDay.Project.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}
