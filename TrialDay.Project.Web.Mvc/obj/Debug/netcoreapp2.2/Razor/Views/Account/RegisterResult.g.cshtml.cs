#pragma checksum "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ea0f8b01c1d9dbe0e3f936dd38d5290f47e9b306"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Account_RegisterResult), @"mvc.1.0.view", @"/Views/Account/RegisterResult.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Account/RegisterResult.cshtml", typeof(AspNetCore.Views_Account_RegisterResult))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\_ViewImports.cshtml"
using Abp.Localization;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ea0f8b01c1d9dbe0e3f936dd38d5290f47e9b306", @"/Views/Account/RegisterResult.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"18197163ec5b28607c8c6054a7fc241fa549c533", @"/Views/_ViewImports.cshtml")]
    public class Views_Account_RegisterResult : TrialDay.Project.Web.Views.ProjectRazorPage<TrialDay.Project.Web.Models.Account.RegisterResultViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
  
    ViewBag.Title = L("SuccessfullyRegistered");

#line default
#line hidden
            BeginContext(121, 54, true);
            WriteLiteral("<div class=\"card\">\n    <div class=\"body\">\n        <h4>");
            EndContext();
            BeginContext(176, 27, false);
#line 7 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
       Write(L("SuccessfullyRegistered"));

#line default
#line hidden
            EndContext();
            BeginContext(203, 60, true);
            WriteLiteral("</h4>\n        <ul>\n            <li><span class=\"text-muted\">");
            EndContext();
            BeginContext(264, 16, false);
#line 9 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                    Write(L("NameSurname"));

#line default
#line hidden
            EndContext();
            BeginContext(280, 9, true);
            WriteLiteral(":</span> ");
            EndContext();
            BeginContext(290, 20, false);
#line 9 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                                              Write(Model.NameAndSurname);

#line default
#line hidden
            EndContext();
            BeginContext(310, 47, true);
            WriteLiteral("</li>\n            <li><span class=\"text-muted\">");
            EndContext();
            BeginContext(358, 16, false);
#line 10 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                    Write(L("TenancyName"));

#line default
#line hidden
            EndContext();
            BeginContext(374, 9, true);
            WriteLiteral(":</span> ");
            EndContext();
            BeginContext(384, 17, false);
#line 10 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                                              Write(Model.TenancyName);

#line default
#line hidden
            EndContext();
            BeginContext(401, 47, true);
            WriteLiteral("</li>\n            <li><span class=\"text-muted\">");
            EndContext();
            BeginContext(449, 13, false);
#line 11 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                    Write(L("UserName"));

#line default
#line hidden
            EndContext();
            BeginContext(462, 9, true);
            WriteLiteral(":</span> ");
            EndContext();
            BeginContext(472, 14, false);
#line 11 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                                           Write(Model.UserName);

#line default
#line hidden
            EndContext();
            BeginContext(486, 47, true);
            WriteLiteral("</li>\n            <li><span class=\"text-muted\">");
            EndContext();
            BeginContext(534, 17, false);
#line 12 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                    Write(L("EmailAddress"));

#line default
#line hidden
            EndContext();
            BeginContext(551, 9, true);
            WriteLiteral(":</span> ");
            EndContext();
            BeginContext(561, 18, false);
#line 12 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
                                                               Write(Model.EmailAddress);

#line default
#line hidden
            EndContext();
            BeginContext(579, 34, true);
            WriteLiteral("</li>\n        </ul>\n        <div>\n");
            EndContext();
#line 15 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
             if (!Model.IsActive)
            {

#line default
#line hidden
            BeginContext(661, 83, true);
            WriteLiteral("                <div class=\"alert alert-warning\" role=\"alert\">\n                    ");
            EndContext();
            BeginContext(745, 32, false);
#line 18 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
               Write(L("WaitingForActivationMessage"));

#line default
#line hidden
            EndContext();
            BeginContext(777, 24, true);
            WriteLiteral("\n                </div>\n");
            EndContext();
#line 20 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
            }

#line default
#line hidden
            BeginContext(815, 1, true);
            WriteLiteral("\n");
            EndContext();
#line 22 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
             if (Model.IsEmailConfirmationRequiredForLogin && !Model.IsEmailConfirmed)
            {

#line default
#line hidden
            BeginContext(917, 83, true);
            WriteLiteral("                <div class=\"alert alert-warning\" role=\"alert\">\n                    ");
            EndContext();
            BeginContext(1001, 30, false);
#line 25 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
               Write(L("WaitingForEmailActivation"));

#line default
#line hidden
            EndContext();
            BeginContext(1031, 24, true);
            WriteLiteral("\n                </div>\n");
            EndContext();
#line 27 "C:\aspnetcore\TrialDay.Project\4.8.0\aspnet-core\src\TrialDay.Project.Web.Mvc\Views\Account\RegisterResult.cshtml"
            }

#line default
#line hidden
            BeginContext(1069, 33, true);
            WriteLiteral("        </div>\n    </div>\n</div>\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TrialDay.Project.Web.Models.Account.RegisterResultViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
