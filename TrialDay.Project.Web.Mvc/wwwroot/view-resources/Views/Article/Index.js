﻿(function () {
	$(function () {

		var _roleService = abp.services.app.role;
		var _articleService = abp.services.app.article;
		var _$modal = $('#ArticleCreateModal');
		var _$form = _$modal.find('form');

		var _$updateform = $('#ArticleEditModal').find('form');

		_$form.validate({
		});

		$('#RefreshButton').click(function () {
			refreshRoleList();
		});

		$('.delete-article').click(function () {
			var articleId = $(this).attr("data-article-id");
			console.log(articleId);
			abp.message.confirm(
				"The Selected Artilce Will Be Deleted.",
				"Are You Sure ?",
				function (isConfirmed) {
					if (isConfirmed) {
						abp.ui.setBusy();
						$.ajax({
							type: "DELETE",
							url: "/api/services/app/ArticleServices/DeleteArticle?Id=" + articleId,
							success: function (data) {
								console.log(data);
								location.reload(true);
								abp.ui.clearBusy();
								abp.notify.success("You have Successfully Deleted an Article");								
							},
							error: function (data) {
								console.log("error");
							}
						});
					}
				}
			);
		});
		$('.edit-article').click(function (e) {
			var articleId = $(this).attr("data-article-id");
			e.preventDefault();
			abp.ajax({
				url: "/api/services/app/ArticleServices/GetArticleByID?Id=" + articleId,
				type: 'GET',
				contentType: 'application/json',
				success: function (data) {
					var _data = data;
					console.log(_data);
					$("#editId").val(_data.id);
					$("#edittitle").val(_data.title);
					$("#editabstract").val(_data.abstract);
					$("#editauthorName").val(_data.authorName);
					$("#editcreationTime").val(_data.creationTime);
					$("#editpublicationDate").val(_data.publicationDate);
					$("#editsource").val(_data.source);
					$("#editdescription").val(_data.description);
				},
				error: function (e) { }
			});
		});

		$("#save").click(function (e) {
			e.preventDefault();
			if (!_$form.valid()) {
				return;
			}

			var article = _$form.serializeFormToObject(); //serializeFormToObject is defined in main.js
			console.log(article);
			abp.ui.setBusy(_$modal);
			$.ajax({
				type: "POST",
				url: "/Article/CreateArticle",
				data: article,
				dataType: "json",
				success: function (data) {
					console.log(data);
					location.reload(true);
					abp.ui.clearBusy();
					abp.notify.success("You have Successfully Created an Article");
				},
				error: function (data) {
					alert("error");
				}
			});
		})

		$("#update").click(function (e) {
			e.preventDefault();
			if (!_$updateform.valid()) {
				return;
			}

			var article = _$updateform.serializeFormToObject(); //serializeFormToObject is defined in main.js
			console.log(article);
			abp.ui.setBusy($('#ArticleEditModal'));
			$.ajax({
				type: "PUT",
				url: "/Article/UpdateArticle",
				data: article,
				dataType: "json",
				success: function (data) {
					abp.notify.success("You have Successfully Update an Article");
					abp.ui.clearBusy();
					location.reload(true);
				},
				error: function (data) {
					alert("error");
				}
			});
		})


		_$modal.on('shown.bs.modal', function () {
			_$modal.find('input:not([type=hidden]):first').focus();
		});

		function refreshRoleList() {
			location.reload(true); //reload page to see new role!
		}
	});
})();
